
let express = require("express");
	//load express module to be able to use express properties and methods
const PORT = 4000;
	//store port 4000 in a variable
let app = express();
	//app is now our server
//middlewares
	//setup for allowing the server to handle data from request(client)
app.use(express.json());
	//allow your app to read json dat
app.use(express.urlencoded({extended:true}));
	//allow your app to read data from the forms

//routes
app.get("/",(req,res)=>res.send(`Hello World`));

//mini activity
	//show a message "Hello from the hello/endpoint"
app.get("/hello",(req,res)=>res.send(`Hello from the hello Endpoint`));

//mini activity
app.post("/greetings",(req,res)=>{
	console.log(req.body); 
		//{firstName: 'Christine'

	res.send(`Hello there, ${req.body.firstName}!`)
	// console.log(`Hello there, !`)
});


app.listen(PORT, ()=>{
	console.log(`Server running at port ${PORT}`)
});